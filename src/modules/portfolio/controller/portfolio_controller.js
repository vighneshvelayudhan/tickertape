const express = require("express");
const router = express.Router();

const portfolioService = require("../service/portfolio_service");

router.get("/", async (req, res, next) => {
  try {
    res.json(await portfolioService.findPortfolio(req));
  } catch (err) {
    next(err);
  }
});
router.get("/returns", async (req, res, next) => {
  try {
    res.json(await portfolioService.findReturn(req));
  } catch (err) {
    next(err);
  }
});

module.exports = router;
