let portfolio = {};
let prices = {
  itc: 200,
  reli: 180,
};
const find = (req) => {
  return portfolio;
};
const setPortfolio = (portfolio) => {
  portfolio = portfolio;
};

const findReturn = (req) => {
  let sum = 0;

  for (const key in portfolio) {
    sum += (prices[key] - portfolio[key].average) * portfolio[key].quantity;
  }

  return sum;
};
module.exports = {
  find,
  setPortfolio,
  findReturn,
};
