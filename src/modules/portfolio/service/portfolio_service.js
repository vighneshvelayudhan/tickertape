const portfolioRepository = require("../repository/portfolio_repository");

const findPortfolio = async (req) => {
  return await portfolioRepository.find(req);
};

const findReturn = async (req) => {
  return await portfolioRepository.findReturn(req);
};

module.exports = {
  findPortfolio,
  findReturn,
};
