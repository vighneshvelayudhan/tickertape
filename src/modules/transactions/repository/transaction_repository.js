let txHistory = [];

const find = (req) => {
  return txHistory;
};

const addTransaction = (reqBody) => {
  const newTx = {
    price: reqBody.price,
    quantity: reqBody.quantity,
    symbol: reqBody.symbol,
    type: reqBody.type,
  };
  txHistory.push(newTx);
};

module.exports = {
  find,
  addTransaction,
};
