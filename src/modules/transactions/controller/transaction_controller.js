const express = require("express");
const router = express.Router();

const transactionService = require("../service/transaction_service");

router.get("/", async (req, res, next) => {
  try {
    res.json(await transactionService.findTransactions(req));
  } catch (err) {
    next(err);
  }
});

router.post("/", async (req, res, next) => {
  try {
    res.json(await transactionService.addTransaction(req));
  } catch (err) {
    console.log(err);
    next(err);
  }
});

module.exports = router;
