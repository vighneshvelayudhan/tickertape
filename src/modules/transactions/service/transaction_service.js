const portfolioRepository = require("../../portfolio/repository/portfolio_repository");
const transactionRepository = require("../repository/transaction_repository");

const findTransactions = async (req) => {
  return await transactionRepository.find(req);
};

const addTransaction = async (req) => {
  if (!req.body.symbol) throw Error("Invalid/Empty Symbol");
  if (!req.body.price || !req.body.quantity || req.body.quantity <= 0)
    throw Error("Invalid parameters");

  const portfolio = portfolioRepository.find();
  if (req.body.type == "sell") {
    if (!portfolio[req.body.symbol]) throw Error("Not holding");
    if (
      portfolio[req.body.symbol].quantity &&
      req.body.quantity > portfolio[req.body.symbol].quantity
    )
      throw Error("Insufficient holdings");

    portfolio[req.body.symbol].quantity -= req.body.quantity;
  } else {
    if (!portfolio[req.body.symbol]) {
      portfolio[req.body.symbol] = {};
      portfolio[req.body.symbol].quantity = 0;
    }

    portfolio[req.body.symbol].average = findAverage(
      portfolio[req.body.symbol].average,
      portfolio[req.body.symbol].quantity,
      req.body.quantity,
      req.body.price
    );

    portfolio[req.body.symbol].quantity += req.body.quantity;
  }
  portfolioRepository.setPortfolio(portfolio);
  transactionRepository.addTransaction(req.body);
};

const findAverage = (currPrice, currQuantity, quantity, price) => {
  currPrice = currPrice ? currPrice : 0;
  currQuantity = currQuantity ? currQuantity : 0;
  const newPrice =
    (currPrice * currQuantity + price * quantity) / (quantity + currQuantity);

  return newPrice;
};

module.exports = {
  findTransactions,
  addTransaction,
};
