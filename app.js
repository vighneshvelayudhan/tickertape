const express = require("express");
const path = require("path");
const logger = require("morgan");
const dotenv = require("dotenv");
const { json, urlencoded } = require("body-parser");
dotenv.config();

const portfolioRouter = require("./src/modules/portfolio/controller/portfolio_controller");
const transactionRouter = require("./src/modules/transactions/controller/transaction_controller");

var app = express();

app.use(logger("dev"));
app.use(json());
app.use(urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, "public")));
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Origin", req.headers.origin);
  //res.header("Access-Control-Allow-Origin", "3bewte.xyz"); // update to match the domain you will make the request from
  res.header(
    "Access-Control-Allow-Methods",
    "GET,PUT,POST,DELETE,PATCH,OPTIONS"
  );
  res.header(
    "Access-Control-Allow-Headers",
    "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization, credentials, Origin"
  );
  if (req.method === "OPTIONS") res.sendStatus(200);
  else next();
});

console.log("initializing routes");
// routes
app.use("/user/portfolio", portfolioRouter);
app.use("/user/transactions/", transactionRouter);

// error handler middleware. Stops the app from crashing on unexpected errors.
app.use((err, req, res, next) => {
  res.status(500).send({ error: err.message });
});

// TODO: make code modular
module.exports = app;
